#include <stdio.h>
#include "ryanxml.h"

static void printTree(struct ryanxml_node * node, int level)
{
   struct ryanxml_node * n;
   enum ryanxml_nodetype type = ryanxml_type(node);
   switch(type)
   {
   case e_rxnt_document:
      printf("[%d] XML Document\n", level);
      break;
   case e_rxnt_declaration:
      printf("[%d] Declaration: <%s attrCount=\"%d\" eleCount=\"%d\">\n", level, ryanxml_elementname(node), node->childAttributeCount, node->childElementCount);
      break;
   case e_rxnt_element:
      printf("[%d] Element: <%s attrCount=\"%d\" eleCount=\"%d\">\n", level, ryanxml_elementname(node), node->childAttributeCount, node->childElementCount);
      break;
   case e_rxnt_elementdata:
      printf("[%d] Text: \"%s\"\n", level, ryanxml_elementdata(node));
      break;
   case e_rxnt_attribute:
      printf("[%d] Attribute: %s=\"%s\"\n", level, ryanxml_attributename(node), ryanxml_attributevalue(node));
      break;
   }
   
   switch(type)
   {
   case e_rxnt_document:
   case e_rxnt_declaration:
   case e_rxnt_element:
      n = node->childAttribute;
      while(n != NULL)
      {
         printTree(n, level + 1);
         n = n->next;
      }
      
      n = node->childElement;
      while(n != NULL)
      {
         printTree(n, level + 1);
         n = n->next;
      }
      break;
   default:
      // Intentionally Left Blank
      break;
   }

}


int main(int argc, char * args[])
{
   struct ryanxml_node * root;

   
   /*
   root = ryanxml_parsestring("<?xml version=\"1.0\" encoding=\"utf-8\"?><test> hi beez/< test2 attribs=\"super\"/></test><hi>hi</hi>", 1);
   printTree(root, 0);
   ryanxml_deletenode(root);
   */
   

   /*
   root = ryanxml_parsestring("<test> hi beez/< test2 attribs=\"super\">one two three</test2> before a <a/> before b <b/> before c <c/> </test>", 1);
   printTree(root, 0);
   ryanxml_deletenode(root);
   */

   
   root = ryanxml_parsefile("test/Freighter1.xml", 0);
   printTree(root, 0);
   ryanxml_deletenode(root);
   
   
   
   
   printf("End\n");   
   return 0;
}

