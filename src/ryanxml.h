/**
 * @file
 *
 * This is the main header for ryanxml. This should be all you need to
 * include. Currently this library only supports reading xml. This library
 * will not convert strings into values, you will need to do that.
 *
 * This library takes xml and builds a tree of the type `struct ryanxml_node`.
 *
 * To get started:
 * @see ryanxml_parsestring
 * @see ryanxml_parsefile
 *
 * 
 */
#ifndef __RYANXML_H__
#define __RYANXML_H__

/** 
 * This is the type of the xml node.
 */
enum ryanxml_nodetype
{
   /** 
    * Usually the root node. This should only contain the xml declaration and
    * the root node. If you don't ignore white spaces this will also include 
    * those.
    */
   e_rxnt_document,
   
   /** 
    * The `<?xml version=\"1.0\" encoding=\"utf-8\"?>` looking like tag.
    * There should only be one of these in your document
    */
   e_rxnt_declaration,

   /** 
    * This is your standard xml tag that could contain attributes or 
    * sub-elements.
    */
   e_rxnt_element,
   /** 
    * This will be the text or data between the tags.
    */
   e_rxnt_elementdata,
   
   /** 
    * Name value pairs for attributes.
    */
   e_rxnt_attribute
};

/** 
 * The main data structure used to hold all of the xml tree information.
 * The xml tree is made up of many of these.
 */
struct ryanxml_node
{
   /** 
    * The pointer to the next sibling of this element.
    * The next child of the parent.
    */
   struct ryanxml_node * next;
   
   /** 
    * The pointer to the previous sibling of this element.
    * The previous child of the parent.
    */
   struct ryanxml_node * prev;
   
   /** 
    * The pointer to the parent element.
    */
   struct ryanxml_node * parent;
   
   /** 
    * The pointer to the first child element of this element
    * The child will have the `prev` field set to NULL and
    * The `next` field will have the second child of this parent if
    * it exists.
    *
    * The child element could be one of the following types:
    * - e_rxnt_attribute
    *
    * This value will only be non-NULL if the `type` parameter is one of the
    * following: 
    * - e_rxnt_document
    * - e_rxnt_element
    */
   struct ryanxml_node * childElement;
   
   /** 
    * The pointer to the first child attribute of this element
    * The child will have the `prev` field set to NULL and
    * The `next` field will have the second child of this parent if
    * it exists.
    *
    * The child element could be one of the following types:
    * - e_rxnt_element
    * - e_rxnt_elementdata
    *
    * This value will only be non-NULL if the `type` parameter is one of the
    * following: 
    * - e_rxnt_document
    * - e_rxnt_element
    */
   struct ryanxml_node * childAttribute;
   
   /** 
    * The name of the current node. This has the following values based on
    * what `type` is:
    * - e_rxnt_document - This will be NULL.
    * - e_rxnt_declaration - This will probably be "xml"
    * - e_rxnt_element - This will be the name of the xml element. For example
    *                    `<name/>` will result in this being set to "name".
    * - e_rxnt_elementdata - This will be NULL.
    * - e_rxnt_attribute - This will be set to the name of the name-value pair
    *                      of the attribute. For example `<test name="value"/>`
    *                      will result in this being set to "name".
    */
   char * name;
   
   /** 
    * The text of the current node. This has the following values based on
    * what `type` is:
    * - e_rxnt_document - This will be NULL.
    * - e_rxnt_declaration - This will be NULL.
    * - e_rxnt_element - This will be NULL.
    * - e_rxnt_elementdata - This will contain the value of the text between 
    *                        nodes. For example `<a>some text</a>` will result
    *                        in this value being "some text"
    * - e_rxnt_attribute - This will be set to the value of the name-value pair
    *                      of the attribute. For example `<test name="value"/>`
    *                      will result in this being set to "value".
    */
   char * text;

   /**
    * The number of child elements this node contains. This only applies to 
    * the following tags:
    * - e_rxnt_document
    * - e_rxnt_element
    */
   int childElementCount;
   
   /**
    * The number of child attributes this node contains. This only applies to 
    * the following tags:
    * - e_rxnt_declaration
    * - e_rxnt_element
    */
   int childAttributeCount; 
   
   /**
    * The type of this node. This determines what fields in this structure 
    * mean.
    */
   enum ryanxml_nodetype type;
};


/** 
 * The structure used to hold information regarding parsing xml (in stream).
 * The details of this are private. You can use this to build node trees from
 * xml data. You can stream in data to this parser in chunks instead of caching
 * the whole document.
 */
struct ryanxml_insteam;

/** 
 * Use this function to setup a new streamed xml document. You will need to
 * clean this up once you are done with it.
 * @see ryanxml_deleteinstream
 *
 * @param[in] ownsDocument               If this is set to a non-zero value the 
 *                                       instream will delete the node tree 
 *                                       once it is deleted.
 * @param[in] keepWhitespaceOnlyElements If this is set to a non-zero value 
 *                                       the parser will add all blocks of text 
 *                                       to the node tree. Even if they are 
 *                                       just newlines or white space.
 * @return a new instrem ready to be fed xml data;
 */
struct ryanxml_instream * ryanxml_newinstream(int ownsDocument, 
                                              int keepWhitespaceOnlyElements);

/** 
 * Use this function to cleanup an instream when you are done with it.
 *
 * Depending on how this instream was configured. It may also delete the
 * node tree structure.
 * @see ryanxml_newinstream
 *
 * @param[in, out] instream  The instream instance you want to delete.
 */
void ryanxml_deleteinstream(struct ryanxml_instream * instream);

/** 
 * Use this function to add xml text to the instream to parse.
 * This can be just a part of the xml text.
 *
 * @param[in,out] instream  The instream instance you want to add new text too.
 * @param[in]     buffer    The xml text you want to add to parse.
 * @param[in]     size      The size in bytes of the `buffer` parameter
 */
void ryanxml_parse(struct ryanxml_instream * instream, const void * buffer, 
                   int size);

/** 
 * This function returns the root (document) node of the instream.
 * You can then traverse the xml tree starting with this node.
 *
 * @param[in,out] instream  The instream instance you want take the root node 
 *                          from.
 * @return The root node of the xml tree based on what was fed into the 
 *         instream. This will have a `type` of e_rxnt_document
 */
struct ryanxml_node * ryanxml_node(struct ryanxml_instream * instream);
/** 
 * This function will delete this node and any child node.
 *
 * @param[in] node  The node to delete
 */
void ryanxml_deletenode(struct ryanxml_node * node);

/** 
 * This function will parse an entire null terminated string and return the 
 * root (document) node of the string you passed in.
 * You will need to cleanup the node after you are done with it.
 * @see ryanxml_deletenode
 *
 * @param[in] text                       The text to parse.
 * @param[in] keepWhitespaceOnlyElements If this is set to a non-zero value 
 *                                       the parser will add all blocks of text 
 *                                       to the node tree. Even if they are 
 *                                       just newlines or white space.
 * @return The root node of the xml tree. This will have a `type` of 
 *         e_rxnt_document
 */
struct ryanxml_node * ryanxml_parsestring(const char * text, 
                                          int keepWhitespaceOnlyElements);

/** 
 * This function will parse and entire file and return the
 * root (document) node of the file you passed in.
 * You will need to cleanup the node after you are done with it.
 * @see ryanxml_deletenode
 *
 * @param[in] filename                   The name of the file you want to parse.
 * @param[in] keepWhitespaceOnlyElements If this is set to a non-zero value 
 *                                       the parser will add all blocks of text 
 *                                       to the node tree. Even if they are 
 *                                       just newlines or white space.
 * @return The root node of the xml tree. This will have a `type` of 
 *         e_rxnt_document. This function will return NULL if the file cant be
 *         opened
 */
struct ryanxml_node * ryanxml_parsefile(const char * filename, 
                                        int keepWhitespaceOnlyElements);


/**
 * This function will search though a tree starting with the given node 
 * looking for the element or attribute node specified by the path.
 *
 * Notes about path:
 * A few examples of path will be the following:
 * - `element/sub-element`
 * - `/element/sub-element`
 * - `/element/sub-element/`
 * - '/element/sub-element@attribute`
 * - '@attribute`
 * - 'element@attribute=value'
 *
 * The path delimits elements with the '/' charater. To find an attribute
 * in the element you can delimit that with a '@' character. You can even
 * match an attribute valuve by using '='. When matching an attribute
 * using '=', if a match is found then the parent element of the matched
 * attribute is returned. 
 * This does not follow XPath even though it looks like it might. You can
 * specify as deep a path as you would like. An empty path is not 
 * currently supported.
 *
 * @param[in] start  The node where the path search will start.
 * @param[in] path   A string containing a path to find.
 * @return This will return a node with the type e_rxnt_element or 
 *         e_rxnt_attribute based on the path parameter. This will return
 *         NULL if it was unable to find the element or attribute specified.
 *
 */
struct ryanxml_node * ryanxml_find(struct ryanxml_node * start, const char * path);

/**
 * This function will search though the tree starting with the given node
 * looking for the element or attribute node specified by the path.
 * If the found node is not an element or a path this function will return
 * NULL. If the found node is an element it will return the text of the 
 * first found child element that is of the type e_rxnt_elementdata or NULL
 * If that child does not exists. If the found node is an attribute then
 * this function will return the text parameter of that node (the attribute
 * value).
 *
 * For details on the path:
 * @see ryanxml_find
 *
 * @param[in] start  The node where the path search will start.
 * @param[in] path   A string containing a path to find.
 * @return The text that is an attribute value, an element child text, or 
 *         NULL if the path could not be found or if the found node does not
 *         have contained text.
 */
const char * ryanxml_findvalue(struct ryanxml_node * start, const char * path);

enum ryanxml_nodetype ryanxml_type(struct ryanxml_node * node);
int ryanxml_isdocument(struct ryanxml_node * node);
int ryanxml_iselement(struct ryanxml_node * node);
int ryanxml_iselementdata(struct ryanxml_node * node);
int ryanxml_isattribute(struct ryanxml_node * node);
const char * ryanxml_elementname(struct ryanxml_node * node);
const char * ryanxml_attributename(struct ryanxml_node * node);
const char * ryanxml_attributevalue(struct ryanxml_node * node);
const char * ryanxml_elementdata(struct ryanxml_node * node);

#endif // __RYANXML_H__

