#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "ryanxml.h"


enum ryanxml_tokentype
{
   e_rxtt_unknown,
   e_rxtt_startoftag,
   e_rxtt_startoftagwithslash,
   e_rxtt_startoftagwithquestionmark,
   e_rxtt_endoftag,
   e_rxtt_endoftagwithslash,
   e_rxtt_endoftagwithquestionmark,
   e_rxtt_word,
   e_rxtt_equals,
   e_rxtt_quote,
   e_rxtt_whitespace
};

enum ryanxml_tagtype
{
   e_rxtt_none,
   e_rxtt_info,
   e_rxtt_open,
   e_rxtt_close
};


struct ryanxml_instream
{
   struct ryanxml_node * document;
   struct ryanxml_node * currentElement;
   struct ryanxml_node * currentAttribute;
   struct ryanxml_node * lastElementChild;
   struct ryanxml_node * lastAttributeChild;
   
   char * bufferBase;
   int bufferSize;
   int bufferIndex;
   
   char * stringBufferBase;
   int stringBufferSize;
   int stringBufferIndex;
   
   enum ryanxml_tagtype tagtype;
   int parserState;
   
   bool ownsDocument;
   bool keepWhitespaceOnlyElements;
};

static struct ryanxml_node * ryanxml_newnode(enum ryanxml_nodetype type)
{
   struct ryanxml_node * node;
   node = malloc(sizeof(struct ryanxml_node));
   node->next = NULL;
   node->prev = NULL;
   node->parent = NULL;
   node->childElement = NULL;
   node->childAttribute = NULL;
   node->childElementCount = 0;
   node->childAttributeCount = 0;
   node->type = type;
   node->text = NULL;
   node->name = NULL;
   return node;

}

#define INITBUFFERSIZE 32

struct ryanxml_instream * ryanxml_newinstream(int ownsDocument, 
                                              int keepWhitespaceOnlyElements)
{
   struct ryanxml_instream * instream;
   instream = malloc(sizeof(struct ryanxml_instream));
   instream->document = ryanxml_newnode(e_rxnt_document);
   instream->currentElement = instream->document;
   instream->currentAttribute = NULL;
   instream->lastElementChild = NULL;
   instream->lastAttributeChild = NULL;

   instream->bufferBase = malloc(INITBUFFERSIZE);
   instream->bufferSize = INITBUFFERSIZE;
   instream->bufferIndex = 0;
   
   instream->stringBufferBase = malloc(INITBUFFERSIZE);
   instream->stringBufferSize = INITBUFFERSIZE;
   instream->stringBufferIndex = 0;
   
   instream->tagtype = e_rxtt_none;
   instream->parserState = 0;
   
   if(ownsDocument)
   {
      instream->ownsDocument = true;
   }
   else
   {
      instream->ownsDocument = false;
   }
   
   if(keepWhitespaceOnlyElements)
   {
      instream->keepWhitespaceOnlyElements = true;
   }
   else
   {
      instream->keepWhitespaceOnlyElements = false;
   }
   return instream;
}

void ryanxml_deleteinstream(struct ryanxml_instream * instream)
{
   if(instream->ownsDocument == true)
   {
      ryanxml_deletenode(instream->document);
   }
   free(instream->bufferBase);
   free(instream->stringBufferBase);
   free(instream);
}

void ryanxml_deletenode(struct ryanxml_node * node)
{
   struct ryanxml_node *iter, *next;

   iter = node->childElement;
   while(iter != NULL)
   {
      next = iter->next;
      ryanxml_deletenode(iter);
      iter = next;  
   }

   iter = node->childAttribute;
   while(iter != NULL)
   {
      next = iter->next;
      ryanxml_deletenode(iter);
      iter = next;  
   }

   if(node->name != NULL)
   {
      free(node->name);
   }

   if(node->text != NULL)
   {
      free(node->text);
   }
   free(node);
}

static void ryanxml_addtobuffer(struct ryanxml_instream * instream, const void * buffer, int size)
{
   int totalSize;
   totalSize = size + instream->bufferIndex;
   if(totalSize > instream->bufferSize)
   {
      instream->bufferSize = totalSize;
      instream->bufferBase = realloc(instream->bufferBase, totalSize);
   }
   memcpy(&instream->bufferBase[instream->bufferIndex], buffer, size);
   instream->bufferIndex = totalSize;
}


static void ryanxml_parsetokenprint(struct ryanxml_instream * instream, 
                                    enum ryanxml_tokentype type, 
                                    int start, int length)
{
   fprintf(stderr, "TokenType: %d\n", (int)type);
   fprintf(stderr, "   Start: %d, length %d\n", start, length);
   fprintf(stderr, "   Text: '%.*s'\n", length, &instream->bufferBase[start]);
}




static void ryanxml_addNewChildElement(struct ryanxml_instream * instream,
                                       struct ryanxml_node * node)
{
   node->next = NULL;
   node->parent = instream->currentElement;
   instream->currentElement->childElementCount ++;
   if(instream->lastElementChild == NULL)
   {
      instream->lastElementChild = node;      
      node->prev = NULL;
      instream->currentElement->childElement = node;
   }
   else
   {
      instream->lastElementChild->next = node;
      node->prev = instream->lastElementChild;
      instream->lastElementChild = node;
   }
   //fprintf(stderr, "Adding New Child Element\n");
}

static void ryanxml_addNewChildAttribute(struct ryanxml_instream * instream,
                                         struct ryanxml_node * node)
{
   node->next = NULL;
   node->parent = instream->currentElement;
   instream->currentElement->childAttributeCount ++;
   if(instream->lastAttributeChild == NULL)
   {
      instream->lastAttributeChild = node;      
      node->prev = NULL;
      instream->currentElement->childAttribute = node;
   }
   else
   {
      instream->lastAttributeChild->next = node;
      node->prev = instream->lastAttributeChild;
      instream->lastAttributeChild = node;
   }
   //fprintf(stderr, "Adding New Child Attribute\n");
}

static void ryanxml_recomputeLast(struct ryanxml_instream * instream)
{
   instream->lastElementChild = instream->currentElement->childElement;
   while(instream->lastElementChild != NULL && 
         instream->lastElementChild->next != NULL)
   {
      instream->lastElementChild = instream->lastElementChild->next;
   }
   instream->lastAttributeChild = instream->currentElement->childAttribute;
   while(instream->lastAttributeChild != NULL && 
         instream->lastAttributeChild->next != NULL)
   {
      instream->lastAttributeChild = instream->lastAttributeChild->next;
   }
}

static void ryanxml_moveToLastChild(struct ryanxml_instream * instream)
{
   instream->currentElement = instream->lastElementChild;
   ryanxml_recomputeLast(instream);
   //fprintf(stderr, "Move to Last Element Child\n");
}

static void ryanxml_moveUp(struct ryanxml_instream * instream)
{
   if(instream->currentElement->parent != NULL)
   {
      instream->currentElement = instream->currentElement->parent;
      ryanxml_recomputeLast(instream);
      //fprintf(stderr, "Move up\n");
   }
   else
   {
      fprintf(stderr, "ryanxml: XML Document is badly formed, Attempted to move above document\n");
   }
}

static char * ryanxml_createString(struct ryanxml_instream * instream,
                                   int start, int length)
{
   char * str;
   str = malloc(length + 1);
   memcpy(str, &instream->bufferBase[start], length);
   str[length] = '\0';
   return str;
}

static bool ryanxml_compareBuffer(struct ryanxml_instream * instream,
                                  int start, int length,
                                  const char * str)
{
   if(str == NULL || strlen(str) != length)
   {
      return false;
   }
   
   if(memcmp(&instream->bufferBase[start], str, length) == 0)
   {
      return true;
   }
   return false;
}

static void ryanxml_appendToStringBuffer(struct ryanxml_instream * instream,
                                         int start, int length)
{
   int totalSize;
   totalSize = instream->stringBufferIndex + length;   
   if(totalSize > instream->stringBufferSize)
   {
      instream->stringBufferBase = realloc(instream->stringBufferBase,
                                           totalSize);
      instream->stringBufferSize = totalSize;
   }
   memcpy(&instream->stringBufferBase[instream->stringBufferIndex],
          &instream->bufferBase[start], length);
   instream->stringBufferIndex = totalSize;
}

static char * ryanxml_copyAndResetStringBuffer(struct ryanxml_instream * instream)
{
   char * str;
   str = malloc(instream->stringBufferIndex + 1);
   memcpy(str, instream->stringBufferBase, instream->stringBufferIndex);
   str[instream->stringBufferIndex] = '\0';
   instream->stringBufferIndex = 0;
   return str;
}

#define ISWHITESPACE(x) ((x) == ' ' || (x) == '\t' || (x) == '\n' || (x) == '\r')

static bool ryanxml_stringBufferHasGoodData(struct ryanxml_instream * instream, 
                                            bool keepWhitespaceOnlyElements)
{
   bool hasNonWhiteSpace;
   int i;
   char c;
   
   if(instream->stringBufferIndex == 0)
   {
      return false;
   }
   
   if(keepWhitespaceOnlyElements == true)
   {
      return true;
   }
   
   hasNonWhiteSpace = false;
   for(i = 0; i < instream->stringBufferIndex; i++)
   {
      c = instream->stringBufferBase[i];
      if(!ISWHITESPACE(c))
      {
         hasNonWhiteSpace = true;
         break;
      }
   }
   
   return hasNonWhiteSpace;
}


static void ryanxml_parsetoken(struct ryanxml_instream * instream, 
                               enum ryanxml_tokentype type, 
                               int start, int length)
{
   struct ryanxml_node * node;
   //fprintf(stderr, "Current State: %d\n", instream->parserState);
   switch(instream->parserState)
   {
      // Looking for next thing to parse
   default:
   case 0:
      switch(type)
      {
      case e_rxtt_startoftag:
      case e_rxtt_startoftagwithslash:
      case e_rxtt_startoftagwithquestionmark:
      
         // Check to see if the string buffer has anything worth saving
         if(ryanxml_stringBufferHasGoodData(instream, instream->keepWhitespaceOnlyElements))
         {
            node = ryanxml_newnode(e_rxnt_elementdata);
            node->text = ryanxml_copyAndResetStringBuffer(instream);
            ryanxml_addNewChildElement(instream, node);
         }
         else
         {
            // Reset the string Buffer
            instream->stringBufferIndex = 0;
         }
      
         // Check to see what kind of tag we are dealing with
         if(type == e_rxtt_startoftag)                  instream->tagtype = e_rxtt_open;
         if(type == e_rxtt_startoftagwithslash)         instream->tagtype = e_rxtt_close;
         if(type == e_rxtt_startoftagwithquestionmark)  instream->tagtype = e_rxtt_info;
         
         // Create a new node as a child if necessary
         if(instream->tagtype == e_rxtt_open)
         {
            ryanxml_addNewChildElement(instream, ryanxml_newnode(e_rxnt_element));            
            ryanxml_moveToLastChild(instream);
         }
         else if(instream->tagtype == e_rxtt_info)
         {
            ryanxml_addNewChildElement(instream, ryanxml_newnode(e_rxnt_declaration));            
            ryanxml_moveToLastChild(instream);
         }
         
         
         // We are now parsing a tag
         instream->parserState = 1;
         break;
      default:
         ryanxml_appendToStringBuffer(instream, start, length);
         break;
      }
      break;
      // Parsing tag looking for name
   case 1:
      if(type == e_rxtt_word)
      {
         if(instream->tagtype == e_rxtt_open ||
            instream->tagtype == e_rxtt_info)
         {
            instream->currentElement->name = ryanxml_createString(instream, start, length);
         }
         else if(instream->tagtype == e_rxtt_close)
         {
            if(ryanxml_compareBuffer(instream, 
                                     start, 
                                     length, 
                                     instream->currentElement->name) == false)
            {
               fprintf(stderr, "ryanxml: Closing Tag for element %s does not match\n",  instream->currentElement->name);
            }
         }

         instream->parserState = 2; // Looking for attributes or closing
      }
      break;
      // Looking for attributes or closing
   case 2:
      if(type == e_rxtt_word)
      {
         instream->currentAttribute = ryanxml_newnode(e_rxnt_attribute);
         ryanxml_addNewChildAttribute(instream, instream->currentAttribute);
         instream->currentAttribute->name = ryanxml_createString(instream, start, length);
         
         instream->parserState = 3; // Looking for an '='         
         
      }
      else if(type == e_rxtt_endoftag ||
              type == e_rxtt_endoftagwithquestionmark)
      {
         if(instream->tagtype == e_rxtt_close ||
            instream->tagtype == e_rxtt_info)
         {
            ryanxml_moveUp(instream);
         }
         
         
         
         instream->parserState = 0; // back to looking for either data or tags
      }
      else if(type == e_rxtt_endoftagwithslash)
      {
         ryanxml_moveUp(instream);
         instream->parserState = 0;
      }      
      break;
   case 3: // looking for equals
      if(type == e_rxtt_equals)
      {
         instream->parserState = 4; // Looking for quote
      }
      break;
      // Looking for open quotation
   case 4:
      if(type == e_rxtt_quote)
      {
         instream->parserState = 5;
      }
      break;
      // Looking for close quotation
   case 5: 
      if(type != e_rxtt_quote)
      {
         ryanxml_appendToStringBuffer(instream, start, length);
      }
      else
      {
         instream->currentAttribute->text = ryanxml_copyAndResetStringBuffer(instream);
         // Looking for attributes or closing
         instream->parserState = 2;
      }
      break;
   }
   
 
}




#define CONSUMETHISONE (index + 1 - start)
#define CONSUMELASTONE (index - start)

static int ryanxml_tokenize(struct ryanxml_instream * instream, 
                            int start,
                            enum ryanxml_tokentype * ptr_type,
                            int * ptr_tokenStart,
                            int * ptr_tokenLength)
{
   int consumed;
   int index;
   int tokenStart;
   int tokenLength;
   enum ryanxml_tokentype type;
   char pc;

   type = e_rxtt_unknown;
   
   index = start;
   consumed = 0;
   pc = '\0';
   while(consumed == 0 && index < instream->bufferIndex)
   {
      char c;
      c = instream->bufferBase[index];
      switch(type)
      {
      default:
      case e_rxtt_unknown:
         if(ISWHITESPACE(c))
         {
            type = e_rxtt_whitespace;
            tokenStart = index;
            index ++;
         }
         else if( c == '<' )
         {
            type = e_rxtt_startoftag;
            tokenStart = index;
            index ++;
         }
         else if(c == '=')
         {
            type = e_rxtt_equals;
            tokenStart = index;
            tokenLength = 1;
            consumed = CONSUMETHISONE;
         }
         else if(c == '"')
         {
            type = e_rxtt_quote;
            tokenStart = index;
            tokenLength = 1;
            consumed = CONSUMETHISONE;
         }
         else if(c == '>')
         {
            type = e_rxtt_endoftag;
            tokenStart = index;
            tokenLength = 1;
            consumed = CONSUMETHISONE;
         }
         else
         {
            type = e_rxtt_word;
            tokenStart = index;
            index ++;
         }
         break;
      case e_rxtt_whitespace:
         if(!ISWHITESPACE(c))
         {
            tokenLength = index - tokenStart;
            consumed = CONSUMELASTONE;
         }
         else
         {
            index ++;
         }
         break;
      case e_rxtt_startoftag:
         if(c == '/')
         {
            type = e_rxtt_startoftagwithslash;
            tokenLength = index + 1 - tokenStart;
            consumed = CONSUMETHISONE;
         }
         else if(c == '?')
         {
            type = e_rxtt_startoftagwithquestionmark;
            tokenLength = index + 1 - tokenStart;
            consumed = CONSUMETHISONE;
         }
         else
         {
            type = e_rxtt_startoftag;
            tokenLength = 1;
            consumed = CONSUMELASTONE;
         }
         break;
      case e_rxtt_word:
         if(c == '>')
         {
            if(pc == '/' ||
               pc == '?')
            {
               tokenLength = index + 1 - tokenStart;
               
               if(tokenLength == 2)
               {
                  if(pc == '/')
                  {
                     type = e_rxtt_endoftagwithslash;
                  }
                  else
                  {
                     type = e_rxtt_endoftagwithquestionmark;
                  }
                  consumed = CONSUMETHISONE;
               }
               else
               {
                  tokenLength = index - 1 - tokenStart;
                  consumed = index - 1 - start;
               }
            }
            else
            {
               tokenLength = index - tokenStart;
               consumed = CONSUMELASTONE;               
            }
         }
         else if(ISWHITESPACE(c) || 
            c == '=' || 
            c == '<' || 
            c == '"')
         {
            tokenLength = index - tokenStart;
            consumed = CONSUMELASTONE;
         }
         else
         {
            index ++;
         }
         break;
      }
      pc = c; // Set Previous character
   }

   if(consumed > 0)
   {
      if(ptr_type != NULL)
      {
         *ptr_type = type;
      }
      if(ptr_tokenStart != NULL)
      {
         *ptr_tokenStart = tokenStart;
      }
      
      if(ptr_tokenLength != NULL)
      {
         *ptr_tokenLength = tokenLength;
      }
      
   }
   return consumed;
}

void ryanxml_parse(struct ryanxml_instream * instream, const void * buffer, int size)
{
   int start, consumed;
   int tokenStart, tokenLength;
   enum ryanxml_tokentype type;
   // add new chunk to the currentElement chunk
   ryanxml_addtobuffer(instream, buffer, size);
  
   start = 0; 
   while((consumed = ryanxml_tokenize(instream, 
                                      start,
                                      &type,
                                      &tokenStart,
                                      &tokenLength)) != 0)
   {
      //fprintf(stderr, "Current State: %d\n", instream->parserState);
      //ryanxml_parsetokenprint(instream, type, tokenStart, tokenLength);
      ryanxml_parsetoken(instream, type, tokenStart, tokenLength);
      //fprintf(stderr, "End State: %d\n", instream->parserState);
      start += consumed;
   }

   if(start > 0)
   {
      instream->bufferIndex -= start;
      memcpy(instream->bufferBase, &instream->bufferBase[start], instream->bufferIndex);
   }
}

struct ryanxml_node * ryanxml_node(struct ryanxml_instream * instream)
{
   return instream->document;
}

struct ryanxml_node * ryanxml_parsestring(const char * text, int keepWhitespaceOnlyElements)
{
   struct ryanxml_instream * instream;
   struct ryanxml_node * node;
   instream = ryanxml_newinstream(0, keepWhitespaceOnlyElements);
   ryanxml_parse(instream, text, strlen(text));
   node = ryanxml_node(instream);
   ryanxml_deleteinstream(instream);
   return node;
}

#define FILEBUFFERSIZE 16

struct ryanxml_node * ryanxml_parsefile(const char * filename, int keepWhitespaceOnlyElements)
{
   struct ryanxml_instream * instream;
   struct ryanxml_node * node;
   char buffer[FILEBUFFERSIZE];
   size_t read;
   FILE * fh;

   fh = fopen(filename, "rb");
   if(fh == NULL)
   {
      fprintf(stderr, "Error: ryanxml could not open file: %s\n", filename);
      return NULL;
   }

   instream = ryanxml_newinstream(0, keepWhitespaceOnlyElements);

   while(!feof(fh))
   {
      read = fread(buffer, 1, FILEBUFFERSIZE, fh);
      ryanxml_parse(instream, buffer, read);
   }
   fclose(fh);
   node = ryanxml_node(instream);
   ryanxml_deleteinstream(instream);
   return node;
}



// ==== User Interface functions ==== //

enum ryanxml_nodetype ryanxml_type(struct ryanxml_node * node)
{
   return node->type;
}

int ryanxml_isdocument(struct ryanxml_node * node)
{
   return node->type == e_rxnt_document;
}

int ryanxml_iselement(struct ryanxml_node * node)
{
   return node->type == e_rxnt_element;
}

int ryanxml_iselementdata(struct ryanxml_node * node)
{
   return node->type == e_rxnt_elementdata;
}

int ryanxml_isattribute(struct ryanxml_node * node)
{
   return node->type == e_rxnt_attribute;
}

const char * ryanxml_elementname(struct ryanxml_node * node)
{
   if(node->type == e_rxnt_element)
   {
      return node->name;
   }
   return NULL;
}

const char * ryanxml_attributename(struct ryanxml_node * node)
{
   if(node->type == e_rxnt_attribute)
   {
      return node->name;
   }
   return NULL;   
}

const char * ryanxml_attributevalue(struct ryanxml_node * node)
{
   if(node->type == e_rxnt_attribute)
   {
      return node->text;
   }
   return NULL;   
}

const char * ryanxml_elementdata(struct ryanxml_node * node)
{
   if(node->type == e_rxnt_elementdata)
   {
      return node->text;
   }
   return NULL;      
}


struct ryanxml_node * ryanxml_find(struct ryanxml_node * start, const char * path)
{
   int index = 0;
   int lastElementIndex = -1;
   int nextStart;
   enum ryanxml_nodetype targetType;
   struct ryanxml_node * n;

   if(start == NULL || path == NULL)
   {
      return NULL;
   }


   if(path[0] == '\0' || (path[0] == '/' && path[1] == '\0'))
   {
      return start;
   }

   if(path[0] == '@')
   {
      targetType = e_rxnt_attribute;
      index ++;
   }
   else if(path[0] == '/')
   {
      targetType = e_rxnt_element;
      index ++;
   }
   else
   {
      targetType = e_rxnt_element;
   }

   
   if(targetType == e_rxnt_element)
   {
      n = start->childElement;
   }
   else
   {
      n = start->childAttribute;
   }
   
   while(n != NULL)
   {
      while(n != NULL && n->type != targetType)
      {
         n = n->next;
      }
      while(n != NULL && memcmp(&path[index], n->name, strlen(n->name)) != 0)
      {
         n = n->next;
         while(n != NULL && n->type != targetType)
         {
            n = n->next;
         }
      }
      if(n == NULL)
      {
         return NULL;
      }
      else
      {
         nextStart = index + strlen(n->name);
         if(path[nextStart] == '\0' || 
            (path[nextStart] == '/' && path[nextStart + 1] == '\0'))
         {
            return n;
         }
         else if(path[nextStart] == '/')
         {
            index = nextStart + 1;;
            n = n->childElement;
         }
         else if(path[nextStart] == '@')
         {
            targetType = e_rxnt_attribute;
            lastElementIndex = index;
            index = nextStart + 1;
            n = n->childAttribute;
         }
         else if(path[nextStart] == '=' && targetType == e_rxnt_attribute)
         {
            // This is for the "@attribute=thing" path
            if(strcmp(&path[nextStart + 1], n->text) == 0)
            {
               // This is a strange case that will return the element
               // instead of the attribute
               return n->parent;
            }
            else
            {
               n = n->parent->next;
               index = lastElementIndex;
               targetType = e_rxnt_element;
            }
         }
         else
         {
            n = n->next;
         }
      }
   }
   return NULL;
}

const char * ryanxml_findvalue(struct ryanxml_node * start, const char * path)
{
   struct ryanxml_node * found;
   struct ryanxml_node * n;

   found = ryanxml_find(start, path);
   if(found == NULL)
   {
      return NULL;
   }
   if(found->type == e_rxnt_attribute)
   {
      return found->text;
   }
   if(found->type != e_rxnt_element)
   {
      return NULL;
   }
   
   n = found->childElement;
   while(n != NULL && n->type != e_rxnt_elementdata)
   {
      n = n->next;
   }

   if(n == NULL)
   {
      return NULL;
   }

   return n->text;
}

