settings = NewSettings()
if family == "windows" then
   settings.cc.defines:Add(" _CRT_SECURE_NO_WARNINGS")
end

settings.cc.includes:Add("src")
settings.cc.includes:Add("test")


source = Collect("src/*.c", "test/*.c")
objects = Compile(settings, source);
exe = Link(settings, "ryanxmltest", objects)

